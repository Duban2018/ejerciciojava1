

import java.util.Scanner;

public class Ejercicio_4 {

	public static void main(String[] args) {
		Ejercicio_4 clase = new Ejercicio_4();
		clase.sexto_ejercicio();
			
	}
	
	public void sexto_ejercicio() {
		Scanner sc = new Scanner (System.in);
		System.out.println("teclea cualquier numero");
		int contador = 0;
		
		while(true){
			int numero = sc.nextInt();
			contador = contador + 1;
			if (numero <0) {
				System.out.println("el numero es negativo");
				break;	
			}
			
		}
	} 		
	
	public void quinto_ejercicio() {
		//indica si es positivo o negativo el numero
				//el proceso se repetira hasta que se introduzca un 0.

				int numero = 0;
				
				
				Scanner sc = new Scanner (System.in);
				
				while (true){
					System.out.println("introduzca un numero(si introduce 0 el programa se terminara)");
					numero = sc.nextInt();
					
					if ( numero >0)System.out.println("su numero es positivo");
					
					if (numero <0)System.out.println("su numero es negativo");
					
					if (numero ==0){ 
						System.out.println("su programa finalizo");
						break;
					}
				
				}
	} 

}
